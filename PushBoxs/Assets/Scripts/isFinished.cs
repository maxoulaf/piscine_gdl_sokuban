﻿using UnityEngine;
using System.Collections;

public class isFinished : MonoBehaviour {

    public GameObject[] goals;
    public string nextLevel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        bool finish = true;
        foreach (GameObject g in goals)
        {
            if (!g.GetComponent<checkGoal>().isActivate())
                finish = false;
        }
        if (finish)
        {
            Application.LoadLevel(nextLevel);
        }
	}
}
