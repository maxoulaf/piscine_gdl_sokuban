﻿using UnityEngine;
using System.Collections;

public class FinishTutorial : MonoBehaviour {

    public GameObject tuto;
    public GameObject littleDoor;
    public GameObject goal;
    public GameObject maze;

    private bool door = false;
	// Update is called once per frame
	void Update () {
        if (goal.GetComponent<checkGoal>().isActivate() && door == false)
        {
            littleDoor.GetComponent<appearMaze>().enabled = true;
            door = true;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        tuto.GetComponent<appearMaze>().enabled = true;
        maze.GetComponent<appearMaze>().enabled = true;
        this.enabled = false;
    }
}
