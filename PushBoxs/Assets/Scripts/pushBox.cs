﻿using UnityEngine;
using System.Collections;

public class pushBox : MonoBehaviour {

    public float pushPower = 2.0f;

	void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;

        if (!body) { return; }

        if (hit.moveDirection.y < -0.3) { return; }

        float dirX = ((Mathf.Abs(hit.moveDirection.x) > Mathf.Abs(hit.moveDirection.z)) ? hit.moveDirection.x : 0.0f);
        float dirZ = ((dirX == 0.0f) ? hit.moveDirection.z : 0.0f);

        Vector3 pushDir = new Vector3(dirX, 0.0f, dirZ);

        RaycastHit hitPoint;

        if (Physics.Raycast(hit.transform.position, -1 * pushDir, out hitPoint) && Input.GetButton("Fire1"))
        {
            int strength = 1;
            if (hitPoint.transform != null && hitPoint.transform.tag == "Player")
            {
                hit.transform.GetComponent<moveBox>().moveOn(pushDir, strength * 2);
            }
        }
    }
}
