﻿using UnityEngine;
using System.Collections;

public class buttonMenu : MonoBehaviour {

    public string name = "";
    private bool loadlvl = false;

    void Update()
    {
        if (loadlvl)
            GameObject.Find("screen Fader").GetComponent<fadeInOut>().EndScene();
    }

    void OnMouseOver()
    {
        transform.localScale = new Vector3(2, 2, 2);
    }

    void OnMouseExit()
    {
        transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
    }

    void OnMouseDown()
    {
        if (name == "jouer")
            loadlvl = true;
        else if (name == "quitter")
            Application.Quit();
    }
}
