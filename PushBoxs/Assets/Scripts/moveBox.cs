﻿using UnityEngine;
using System.Collections;

public class moveBox : MonoBehaviour {

    private Vector3 startMove;
    private Vector3 endMove;
    private bool    isMoving;
    private float lerp;

    public float speed = 3.0f;

	// Use this for initialization
	void Start () {
        isMoving = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (isMoving)
        {
            lerp += speed * Time.deltaTime;
            lerp = Mathf.Clamp(lerp, 0.0f, 1.0f);
            transform.position = Vector3.Lerp(startMove, endMove, lerp);
            if (lerp == 1.0f)
                isMoving = false;
        }
	}

    public void moveOn(Vector3 pushDir, int strength)
    {
        if (!isMoving)
        {
            Vector3 dir = new Vector3(Mathf.Round(pushDir.x), Mathf.Round(pushDir.y), Mathf.Round(pushDir.z));
            startMove = transform.position;
            endMove = startMove + ((float)strength * dir);

            RaycastHit hit;

            if (!Physics.Raycast(startMove, endMove - startMove, out hit, strength) || hit.collider.isTrigger)
            {
                isMoving = true;
                lerp = 0.0f;
            }
        }
    }
}
