﻿using UnityEngine;
using System.Collections;

public class apearText : MonoBehaviour {



    private float lerp;
    private bool quit = true;

	// Use this for initialization
	void Start () {
        transform.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
        lerp = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {
        if (lerp <= 1.0f)
        {
            if (quit)
            {
                transform.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f - lerp);
            }
            else
            {
                transform.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, lerp);
            }
            lerp += Time.deltaTime;
        }
	}

    void OnTriggerEnter(Collider coll)
    {
        if (coll.transform.tag == "Player")
        {
            quit = false;
            lerp = 0.0f;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.transform.tag == "Player")
        {
            quit = true;
            lerp = 0.0f;
        }
    }
}
