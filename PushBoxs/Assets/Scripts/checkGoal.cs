﻿using UnityEngine;
using System.Collections;

public class checkGoal : MonoBehaviour {

    public Color desactivate;
    public Color activate;

    private bool isOk = false;

	// Update is called once per frame
	void Update () {
        RaycastHit  hit;

        if (Physics.Raycast(transform.position + (6.0f * Vector3.up), Vector3.down, out hit, 2.0f))
        {
            if (hit.transform.tag == "Cube")
            {
                isOk = true;
                gameObject.renderer.material.SetColor("_Spec", activate);
            }
        }
        else
        {
            gameObject.renderer.material.SetColor("_Spec", desactivate);
            isOk = false;
        }
	}

    public bool isActivate()
    {
        return (isOk);
    }
}
