﻿using UnityEngine;
using System.Collections;

public class appearMaze : MonoBehaviour {

    public float speed = 0.4f;
    public GameObject player;

    private float lerp;
    private Vector3 startPosition;
    public Vector3 endPosition = new Vector3(0, 0, 0);

    void    Start()
    {
        startPosition = transform.position;
        lerp = 0.0f;
        player.GetComponent<CharacterMotor>().enabled = false;
    }

	// Update is called once per frame
	void Update () {
        if (lerp <= 1.0f)
        {
            lerp += speed * Time.deltaTime;
            transform.position = Vector3.Lerp(startPosition, endPosition, lerp);
        }
        if (lerp > 1.0f)
        {
            player.GetComponent<CharacterMotor>().enabled = true;
            this.enabled = false;
        }
	}
}
