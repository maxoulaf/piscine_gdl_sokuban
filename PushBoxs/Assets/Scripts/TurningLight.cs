﻿using UnityEngine;
using System.Collections;

public class TurningLight : MonoBehaviour {

    public float speed = 10.0f;
	
	// Update is called once per frame
	void Update () {
        transform.RotateAroundLocal(new Vector3(0, 1, 0), Time.deltaTime * speed);
	}
}
